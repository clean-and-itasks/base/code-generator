# Changelog

### v2.4.0

- Feature: in `ccall` for intel/amd allow multiple results by passing a single pointer (into the B stack) to C,
  the results must have at least the size of a word (a B stack element) and are enclosed in ()'s in the `ccall` string,
  the C code should store the results in successive memory locations (starting with the first),
  the code generator does not take into account padding by the C compiler, for example: `ccall` three_results "p:V(ppp)"
- Feature: for intel/amd processors add abc instructions `cselect_b` (conditionally select an integer, using a
  conditional move instruction), `ctzb` (count trailing zero bits, using `bsf` or `tzcnt` instead if bit manipulation
  instructions are allowed (`-bm`)), and `cnzb` (count non zero bits, using `popcnt`).
- Enhancement: optimise generated code for converting a condition code to `1` or `0` for intel/amd processors.
- Enhancement: optimisation for intel/amd processors, use `lea` instruction instead of a move and an add or sub of a
  constant instruction.

### v2.3.0

- Feature: skip `.r` directives. (They are not used by this code generator.)
- Feature: add `-bm` flag to enable new bit manipulation instructions on
  amd64: lzcnt, andn, sarx, shlx and shrx.
- Enhancement: optimise generated amd64 code for eqAC_a of strings with
  length<=16.
- Fix: fix 32 bit register names in generated amd64 assembly of registers 10-15.
- Fix: generated code for 32 bit integer array update with a small constant
  index without checking indices for 64 bit Intel/AMD.

### v2.2.0

- Feature: add support for instructions `eq_arrayp2size`, `empty_arrayp2`,
  `copy_array_next`, `add_hp_arrayp2_first`, `add_hp_arrayp2_next`, and
  `set_arraysize`.
- Enhancement: reduce memory usage and improve performance of small array
  constants.
- Enhancement: optimize generated address calculation code for unboxed arrays
  of records with 7, 14, 15, 23, or 27–31 record elements.

### v2.1.0

- Feature: add support for `create_arrayp2` and `create_arrayp2_` instructions.
- Fix: fix `.array` directive to use the new array representation.

## v2.0

- Enhancement: add dedicated `_ARRAY_` descriptors for unboxed arrays of `Int`,
  `Real`, `Bool`, 32-bit `Int`, and 32-bit `Real`, to save space. Add
  `_ARRAY_R_` descriptor for unboxed arrays of records.

## v1.0

First tagged version.
