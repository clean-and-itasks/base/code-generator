# code-generator

This is the repository of the [Clean][] code generator.

This is a delayed mirror of the [upstream][] version and is only used to
publish the package. Periodically changes from upstream are released in a new
version here.

The code generator is released in the `base-code-generator` package. This
package should normally not be used directly; instead, you should use `base`.

See the documentation in [base][] if you intend to make a merge request for
this repository.

## Maintainer & license

This mirror is maintained by [Camil Staps][]. The upstream is maintained by
John van Groningen.

For license details, see the [LICENSE](/LICENSE) file.

[base]: https://gitlab.com/clean-and-itasks/base/base
[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[upstream]: https://gitlab.science.ru.nl/clean-compiler-and-rts/code-generator
